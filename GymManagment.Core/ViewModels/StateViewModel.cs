﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class StateViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string CountryId { get; set; }
		[Display(Name ="Country")]
		public string CountryName { get; set; }
		public bool IsActive { get; set; }
	}
}
