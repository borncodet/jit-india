﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class CountryViewModel
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public bool IsActive { get; set; }
	}
}
