﻿using GymManagment.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Core.ViewModels
{
	public class WebsiteFooterViewModel
	{
		public GeneralSettings AboutUs { get; set; }
		public List<BlogViewModel> Blogs { get; set; }
	}
}
