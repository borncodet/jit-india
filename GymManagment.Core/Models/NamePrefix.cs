﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GymManagment.Core.Models
{
	public class NamePrefix
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int NamePrefixId { get; set; }
		public string NamePrefixText { get; set; }
	}
}
