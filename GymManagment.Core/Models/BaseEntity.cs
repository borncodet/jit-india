﻿
using GymManagment.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GymManagment.Core.Models
{
	public abstract class BaseEntity : AuditableEntity, IBaseEntity, IActiveEntity
	{
		[Key, Column(Order = 0)]
		[MaxLength(256)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public string Id { get; set; }

		[Required]
		public bool IsActive { get; set; }
	}
}
