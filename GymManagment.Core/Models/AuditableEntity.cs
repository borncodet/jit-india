﻿using GymManagment.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GymManagment.Core.Models
{
	public class AuditableEntity : IAuditableEntity
	{
		public string CreatedBy { get; set; }

		[ForeignKey("CreatedBy")]
		[NotMapped]
		public virtual ApplicationUser CreatedByUser { get; set; }

		public string UpdatedBy { get; set; }
		[ForeignKey("UpdatedBy")]
		[NotMapped]
		public virtual ApplicationUser UpdatedByUser { get; set; }

		[Required]
		public DateTime CreatedOn { get; set; }

		public DateTime? UpdatedOn { get; set; }

		public AuditableEntity()
		{
			CreatedOn = DateTime.Now;
		}
	}
}
