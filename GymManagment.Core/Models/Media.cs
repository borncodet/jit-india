﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GymManagment.Core.Models
{
	[Table(name:"Medias")]
	public class Media:BaseEntity
	{
		public string FileName { get; set; }
		public string Extension { get; set; }
		public string ContentType { get; set; }
		public long ContentLength { get; set; }
	}
}
