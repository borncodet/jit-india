﻿using GymManagment.Shared.Enums;
using GymManagment.Shared.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GymManagment.Core.Models
{
    public class ApplicationUser : IdentityUser, IAuditableEntity, IHostEntity, IActiveEntity, IDeviceEntity
    {

        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public bool IsLockedOut => LockoutEnabled && LockoutEnd >= DateTimeOffset.UtcNow;

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public DeviceType DeviceType { get; set; }
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }

        public string Host { get; set; }

        public bool IsActive { get; set; }

        public int UserTypeId { get; set; }

        public string AccessCardCode { get; set; }

        public virtual ICollection<IdentityUserRole<string>> Roles { get; set; }
        public virtual ICollection<IdentityUserClaim<string>> Claims { get; set; }
    }
}
