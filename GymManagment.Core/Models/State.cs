﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.Models
{
	public class State : BaseEntity
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public string CountryId { get; set; }
		public virtual Country Country { get; set; }
	}
}
