﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.PostModels
{
	public class CountryPostModel
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public bool IsActive { get; set; }
	}
}
