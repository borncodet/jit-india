﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GymManagment.Core.PostModels
{
	public class StatePostModel
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public string CountryId { get; set; }
		[Required]
		public bool IsActive { get; set; }
	}
}
