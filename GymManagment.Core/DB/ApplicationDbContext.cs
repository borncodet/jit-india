﻿using GymManagment.Core.Models;
using GymManagment.Shared.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GymManagment.Core
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public string CurrentUserId { get; set; }
        public ILoggerFactory LoggerFactory { get; }
        public IHttpContextAccessor ContextAccessor { get; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            LoggerFactory = this.GetService<ILoggerFactory>();
            ContextAccessor = this.GetService<IHttpContextAccessor>();
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Enquiry> Enquiries { get; set; }
        public DbSet<Media> Medias { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Workout_Activity> Workout_Activities { get; set; }
        public DbSet<Workout_Cardio> Workout_Cardios { get; set; }
        public DbSet<Workout_Cooldown> Workout_Cooldowns { get; set; }
        public DbSet<Workout_Excercise> Workout_Excercises { get; set; }
        public DbSet<Workout_Intensity> Workout_Intensities { get; set; }
        public DbSet<Workout_Strength> Workout_Strengths { get; set; }
        public DbSet<Workout_Warmup> Workout_Warmups { get; set; }
        public DbSet<GeneralSettings> GeneralSettings { get; set; }
        public DbSet<CMSContent> CMSContents { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }

        public Task<int> SaveChangesAsync()
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync();
        }

        private void UpdateAuditEntities()
        {
            IEnumerable<EntityEntry> modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (EntityEntry entry in modifiedEntries)
            {
                IAuditableEntity entity = (IAuditableEntity)entry.Entity;
                DateTime now = DateTime.Now;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedOn = now;
                    entity.CreatedBy = CurrentUserId ?? string.Empty;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedOn).IsModified = false;
                    entity.UpdatedBy = CurrentUserId ?? string.Empty;
                    entity.UpdatedOn = DateTime.Now;
                }
                if (entry.Entity is IHostEntity hostEntity)
                {
                    hostEntity.Host = GetClientIp();
                }
            }
        }

        private string GetClientIp()
        {
            return Convert.ToString(ContextAccessor.HttpContext?.Connection?.RemoteIpAddress);
        }
    }
}
