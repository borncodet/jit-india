﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class CertificateModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public CertificateModel(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public IList<CMSContentViewModel> CertificateSection { get; set; }

        public async Task OnGetAsync()
        {
            CertificateSection = (await _cmsRepository.GetCMSContent("CertificateSection")).ToList();
        }
    }
}
