﻿
var total = 0, totalAmountReceived = 0, balance = 0, previousBalance = 0, discountAmount = 0;
var servicePeriodId, needQuantityField, fee, duration, startDate, endDate, quantity, netFee, durationText, servicePeriodName;

//#region Membership Category

$("#cmbMembershipCategory").change(function () {

    if ($("#txtMembershipNo").val() == '') {
        errorMessage("Please select a Member first");
        $("#txtMembershipNo").focus();
        return;
    }

    for (var i = 0; i < packages.length; i++) {
        var item = packages[i];
        if (item["MembershipCategoryId"] == $("#cmbMembershipCategory").val()) {

            servicePeriodId = item["ServicePeriodId"];
            needQuantityField = item["NeedQuantityField"];
            fee = parseFloat(item["Fee"]);
            duration = item["Duration"];
            servicePeriodName = item["ServicePeriodName"];
            $("#lblQuantity").text(item["ServicePeriodName"] + " Count");

            if (!needQuantityField) {
                $("#divQuantity").hide();
            }
            else {
                $("#divQuantity").show();
                $("#txtQuantity").val('1');
            }
            $("#txtFee").val(fee);
            break;
        }
    }

    var obj = {
        "CustomerId": customerId,
        "MembershipCategoryId": $("#cmbMembershipCategory").val()
    };

    Post("membership/get-customer-service-start-date", obj, "setStartDate");

});

setStartDate = function (data) {
    startDate = new Date(data);
    $("#txtStartDate").val(startDate.toDateInputValue());
    setEndDate();
};

$("#txtStartDate").blur(function () {
//    if (new Date($("#txtStartDate").val()) < startDate) {
//        $("#txtStartDate").val(startDate.toDateInputValue());
//    }
   setEndDate();
});

$("#txtQuantity").change(function () {
    setEndDate();
});

setEndDate = function () {

    $("#divEndDate").show();
    endDate = new Date($('#txtStartDate').val());

    if (!needQuantityField) {
        quantity = 1;
    }
    else {
        quantity = parseFloat($('#txtQuantity').val());
    }

    switch (servicePeriodId) {
        case "1":
            endDate.setMonth(endDate.getMonth() + (duration * quantity));
            endDate.setDate(endDate.getDate() - 1);
            break;
        case "2":
            endDate.setDate(endDate.getDate() + (7 * duration * quantity) - 1);
            break;
        case "3":
            endDate.setDate(endDate.getDate() + (duration * quantity) - 1);
            break;
        case "4":
            $("#divEndDate").hide();
    }
    netFee = fee * quantity;
    $('#txtEndDate').val(endDate.toDateInputValue());
    $("#txtFee").val(netFee);
    durationText = (duration * quantity) + " " + servicePeriodName;
}


//#endregion

//#region Add Button

$("#btnAdd").click(function () {

    //#region validation
    //clearBorder();
    var cntr = [];
    cntr.push($("#cmbMembershipCategory"));
    if (needQuantityField)
        cntr.push($("#txtQuantity"));
    cntr.push($("#txtStartDate"));
    if (servicePeriodId != 4)
        cntr.push($("#txtEndDate"));
    if (!isModelStateValid(cntr))
        return;

    //#endregion

    var slNo = $('#tblItem tr').length;

    var newItem = `<tr>
            <td><a onclick='removeRow(this)'><span class="glyphicon glyphicon-trash"></span></a></td>
            <td>
                <input type="hidden" name="Data.Items[${slNo - 1}].MembershipCategoryId" value="${$("#cmbMembershipCategory").val()}"/>
                <input type="text" class="form-control" name="Data.Items[${slNo - 1}].MembershipCategoryName" value="${$("#cmbMembershipCategory option:selected").text()}" readonly/>
            </td>
            <td>
                <input type="hidden" name="Data.Items[${slNo - 1}].Quantity" value="${quantity}"/>
                ${durationText}
            </td>
            <td>
                <input type="text" class="form-control" name="Data.Items[${slNo - 1}].StartDate" value="${$("#txtStartDate").val()}" readonly/>
            </td>
            <td>
                <input type="text" class="form-control" name="Data.Items[${slNo - 1}].EndDate" value="${$("#txtEndDate").val()}" readonly/>
            </td>
            <td>
                <input type="text" class="form-control" name="Data.Items[${slNo - 1}].Fee" value="${$("#txtFee").val()}" id="Data_Items_${slNo - 1}__Fee" readonly/>
            </td>
            
            </tr>`;
    $("#tblItem").append(newItem);

    clearStrip();
    calculateTotal();
    $("#cmbMembershipCategory").focus();
});

clearStrip = function () {
    $("#cmbMembershipCategory").val(null);
    $("#txtQuantity").val('1');
    $("#txtStartDate").val('');
    $("#txtEndDate").val('');
    $("#txtFee").val('');
    $("#btnAdd").val('Add');
}


removeRow = function (selector) {
    if ($('#tblItem tr').length > 1) {
        $(selector).closest('tr').remove();
        var itemIndex = -1;
        var slNo = 0;
        $('#tblItem tr').each(function () {
            var this_row = $(this);
            this_row.find('input[name$=".MembershipCategoryId"]').attr('name', 'Data_Items[' + itemIndex + '].MembershipCategoryId');
            this_row.find('input[name$=".StartDate"]').attr('name', 'Data_Items[' + itemIndex + '].StartDate');
            this_row.find('input[name$=".EndDate"]').attr('name', 'Data_Items[' + itemIndex + '].EndDate');
            this_row.find('input[name$=".Quantity"]').attr('name', 'Data_Items[' + itemIndex + '].Quantity');
            this_row.find('input[name$=".Fee"]').attr('name', 'Data_Items[' + itemIndex + '].Fee');
            this_row.find('input[name$=".MembershipCategoryName"]').attr('name', 'Data_Items[' + itemIndex + '].MembershipCategoryName')
            this_row.find('input[name$=".MembershipCategoryName"]').attr('name', 'Data_Items[' + itemIndex + '].MembershipCategoryName');
            this_row.find('input[name$=".Fee"]').attr('id', `Data_Items_${itemIndex}__Fee`);
            itemIndex++;
            slNo++;
        });
    }
    calculateTotal();
}

//#endregion
