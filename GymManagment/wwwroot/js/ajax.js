﻿Post = function (apiName, paramArray, successFun, reference) {
    $.ajax({
        type: "POST",
        url: "api/" + apiName,
        data: JSON.stringify(paramArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window[successFun](data, reference);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

PostAlreadyExist = function (apiName, parameter, successFun, message, element) {
    $.ajax({
        type: "POST",
        url: "api/" + apiName + parameter,
        contentType: "application/json;",
        dataType: "json",
        success: function (data) {
            if (parseInt(data) > 0) {
                window[successFun](message, element)
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

PostWithResponse = function (apiName, paramArray, successFun, reference) {
    $.ajax({
        type: "POST",
        url: "api/" + apiName,
        data: JSON.stringify(paramArray),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.Response.IsError) {
                errorMessage(data.Response.Response);
                return;
            }
            window[successFun](data, reference);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}

PostForm = function (evt, apiName, formId, successFun, msgDivId, msgId) {

    evt.preventDefault();
    var form = $(formId)[0];
    var formData = new FormData(form);
    //debugger;
    if (!$(formId).valid()) return false;
    $.ajax({
        url: apiName,
        processData: false,
        contentType: false,
        data: formData,
        type: 'POST'
    }).done(function (result) {
        window[successFun](result);
    }).fail(function (a, b, c) {
        errorMessage("Error Occured", msgDivId, msgId);
    });
}

PostFormWithResponse = function (evt, apiName, formId, successFun, msgDivId, msgId) {

    evt.preventDefault();
    var form = $(formId)[0];
    var formData = new FormData(form);

    if (!$(formId).valid()) return false;
    $.ajax({
        url: apiName,
        processData: false,
        contentType: false,
        data: formData,
        type: 'POST'
    }).done(function (result) {
        if (result.Response.IsError) {
            errorMessage(result.Response.Response, msgDivId, msgId);
            return;
        }
        window[successFun](result);
    }).fail(function (a, b, c) {
        errorMessage(result.Response.Response, msgDivId, msgId);
    });
}

uploadImage = async function (inputId, imageType, successFun) {


    var input = document.getElementById(inputId);
    var files = input.files;
    var formData = new FormData();

    for (var i = 0; i != files.length; i++) {
        formData.append("files", files[i]);
    }
    formData.append("ImageType", imageType);

    $.ajax({
        url: '/api/upload/post',
        processData: false,
        contentType: false,
        data: formData,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('request-from', 'WEB');
            xhr.setRequestHeader('Authorization', 'Bearer ' + getToken());
        },
        type: 'POST'
    }).done(function (result) {
        window[successFun](result);
    }).fail(function (a, b, c) {
        console.log(a, b, c);
    });
}



getToken = function () {
    return window.localStorage.getItem("token");
}

setToken = function (token) {
    window.localStorage.setItem("token", token);
}

removeToken = function () {
    window.localStorage.removeItem("token");
}
