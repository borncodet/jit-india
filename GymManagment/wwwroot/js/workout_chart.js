﻿var masterId = "";
var warmups = [], strengths = [], cardios = [], cooldowns = [];
var lastday = 0;

$(function () {

	//#region Page Load

	$(document).ready(function () {
		masterId = getIdFromURL();
		Post("workout/chart", masterId, "formLoading");
	});

	formLoading = function (data) {
		loadCombos(data);
	}

	loadCombos = function (data) {
		fillMasterData(data.Master);
		warmups = data.Warmups;
		strengths = data.Strengths;
		cardios = data.Cardios;
		cooldowns = data.Cooldowns;
		fillChart(data.MinAndMaxDay);
	}

	//#endregion

	//#region Master
	fillMasterData = function (data) {
		$("#divDateFrom").text(new Date(data.FromDate).toDateInputValue());
		$("#divDateTo").text(new Date(data.ToDate).toDateInputValue());
		$("#divGoal").text(data.Goal);
	}

	fillChart = function (data) {
		divChart = "";
		for (var day = data.MinDay; day <= data.MaxDay; day++) {

			lastday = 0;

			divChart += `<div style="float:right; width:100px; font-size:16px; font-weight:bold; padding:8px 0px 0px 0px; text-transform:uppercase;">Days: ` + day + ` </div>`;
			$.each(warmups, function () {
				var entry = this;
				if (entry.DayFrom <= day && entry.DayTo >= day) {

					if (lastday != day) {
						divChart += `
						<h3 style="font-size: 18px !important;color: #fff !important;margin: 0px 0px 0px 0px;padding: 4px 0px 4px 15px !important;text-align: left;font-weight: 600;background: #808080;float: left;text-transform: uppercase;">
							Warm Up </h3>
						
						<div class="clearfix"></div>
						<div class="workout">
							<div class="table-responsive ">
								<table width="100%" class="table-striped table-bordered">
									<thead>
										<tr>
											<th>Activity </th>
											<th>Time / Dist</th>
											<th>Sets/ Reps  </th>
											<th>Intensity  </th>
											<th>Notes </th>
										</tr>
									</thead>
									<tbody>`;

					}

					divChart += `<tr>
										<td>`+ entry.Activity+`</td>
										<td>`+ entry.TimeDist +`</td>
										<td>`+ entry.SetsReps +`</td>
										<td>`+ entry.Intensity +`</td>
										<td>`+ entry.Notes +`</td>
									</tr>`;

					if (lastday != day) {
						divChart += `</tbody>
							</table>
						</div>
					</div>`
					};
					lastday = day;
				}
			});

			lastday = 0;
			$.each(strengths, function () {
				var entry = this;
				if (entry.DayFrom <= day && entry.DayTo >= day) {

					if (lastday != day) {
						divChart += `
						<h3 style="font-size: 18px !important;color: #fff !important;margin: 0px 0px 0px 0px;padding: 4px 0px 4px 15px !important;text-align: left;font-weight: 600;background: #808080;float: left;text-transform: uppercase;">
									Strength Training
								</h3>
						<div class="workout">
							<div class="table-responsive ">
								<table width="100%" class="table-striped table-bordered">
									<thead>
										<tr>
											<th>Exercises </th>
											<th>Sets/ Reps</th>
											<th>Weight</th>
											<th>Rest Time  </th>
											<th>Notes </th>
										</tr>
									</thead>
									<tbody>`;

					}

					divChart += `<tr>
										<td>`+ entry.Excercise + `</td>
										<td>`+ entry.SetsReps + `</td>
										<td>`+ entry.Weight + `</td>
										<td>`+ entry.RestTime + `</td>
										<td>`+ entry.Notes + `</td>
									</tr>`;

					if (lastday != day) {
						divChart += `</tbody>
							</table>
						</div>
					</div>`
					};
					lastday = day;
				}
			});

			lastday = 0;
			$.each(cardios, function () {
				var entry = this;
				if (entry.DayFrom <= day && entry.DayTo >= day) {

					if (lastday != day) {
						divChart += `
						<h3 style="font-size: 18px !important;color: #fff !important;margin: 0px 0px 0px 0px;padding: 4px 0px 4px 15px !important;text-align: left;font-weight: 600;background: #808080;float: left;text-transform: uppercase;">
									Cardio Training
								</h3>
						<div class="workout">
							<div class="table-responsive ">
								<table width="100%" class="table-striped table-bordered">
									<thead>
										<tr>
											<th>Exercises </th>
											<th>Time / Dist</th>
											<th>Target HR  </th>
											<th>Intensity  </th>
											<th>Notes </th>
										</tr>
									</thead>
									<tbody>`;

					}

					divChart += `<tr>
										<td>`+ entry.Excercise + `</td>
										<td>`+ entry.TimeDist + `</td>
										<td>`+ entry.TargetHR + `</td>
										<td>`+ entry.Intensity + `</td>
										<td>`+ entry.Notes + `</td>
									</tr>`;

					if (lastday != day) {
						divChart += `</tbody>
							</table>
						</div>
					</div>`
					};
					lastday = day;
				}
			});

			lastday = 0;
			$.each(cooldowns, function () {
				var entry = this;
				if (entry.DayFrom <= day && entry.DayTo >= day) {

					if (lastday != day) {
						divChart += `
						<h3 style="font-size: 18px !important;color: #fff !important;margin: 0px 0px 0px 0px;padding: 4px 0px 4px 15px !important;text-align: left;font-weight: 600;background: #808080;float: left;text-transform: uppercase;">
									Cool Down
								</h3>
						<div class="workout">
							<div class="table-responsive ">
								<table width="100%" class="table-striped table-bordered">
									<thead>
										<tr>
											<th>Activity </th>
											<th>Time / Dist</th>
											<th>Sets/ Reps  </th>
											<th>Intensity  </th>
											<th>Notes </th>
										</tr>
									</thead>
									<tbody>`;

					}

					divChart += `<tr>
										<td>`+ entry.Activity + `</td>
										<td>`+ entry.TimeDist + `</td>
										<td>`+ entry.SetsReps + `</td>
										<td>`+ entry.Intensity + `</td>
										<td>`+ entry.Notes + `</td>
									</tr>`;

					if (lastday != day) {
						divChart += `</tbody>
							</table>
						</div>
					</div>`
					};
					lastday = day;
				}
			});

			divChart +="<div style='page-break-after:always;'></div>"
		}

		$("#divChart").html(divChart);
	}


	//#endregion

});