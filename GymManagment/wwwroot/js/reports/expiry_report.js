﻿


$(function () {
	
	$('#btnShowReport').on('click', function (evt) {
		PostForm(evt, '/api/report/expiry-report', '#formBasic', "showReport");
	});

	showReport = function (data) {
		$("#tblGrid tbody").empty();
		var entries = "";
		var previosExpiresOn="";
		$.each(data, function () {
			var entry = this;
			entries = entries +`<tr>`;
			if (previosExpiresOn != entry.ExpiresOn) {
				previosExpiresOn = entry.ExpiresOn;
				entries = entries +
					`<td>${entry.ExpiresOn}</td>`;
			}
			else {
				entries = entries + `<td></td>`;
			}
			entries = entries + `
				<td>${entry.MembershipNo}</td>
				<td>${entry.CustomerName}</td>
				<td>${entry.MembershipCategoryName}</td>
				<td>${entry.Phone}</td>
			</tr>`;
		});

		if (data.length == 0) {
			entries = `<tr><td colspan='4'>No Items Found</td></tr>`;
		}

		$("#tblGrid tbody").append(entries);
	}

});