﻿$(function () {

    //Abhilash C K
    //evt - event
    //dtId - datatable id
    //formId - form id
    //apiName - api name
    LoadDatatable = function (evt, dtId, formId, apiName) {

        evt.preventDefault();

        // Get form
        var form = $(formId)[0];

        // Create an FormData object
        var param = new FormData(form);

        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            data: param,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (result, textStatus, jqXHR) {
                //clear datatable if datatable is not empty
                if ($(dtId).DataTable().data().any()) {
                    $(dtId).empty();
                }

                //Check the length of the result data from api
                if (result == undefined || result == null || result.length == 0) {
                    $(dtId).DataTable({
                        "oLanguage": {
                            "sEmptyTable": "No Data Found."
                        },
                        "destroy": true,
                        "bLengthChange": false
                    });
                }
                else {

                    //get property names from first object of an array of object
                    var keys = Object.keys(result[0]);

                    var jsonObj = [];
                    item = {};
                    item["data"] = 'id';
                    item["defaultContent"] = '';
                    item["sortable"] = false;
                    jsonObj.push(item);

                    var i = 0;
                    //iterate over the property names to create columns entry of datatable
                    keys.forEach(function (value) {

                        i += 1;
                        item = {}
                        item["data"] = value;
                        item["autoWidth"] = true;

                        jsonObj.push(item);

                        //if the column creation of datatable is completed
                        if (i == keys.length) {

                            var columns = JSON.stringify(jsonObj);

                            console.log(columns);

                            var table = $(dtId).DataTable({
                                "aaData": result,
                                "destroy": true,
                                "columns": jsonObj,
                                "order": [],
                                "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, 'All']],
                                "columnDefs": [
                                    { orderable: false, targets: [0] }
                                ],
                                "dom": 'Blfrtip',
                                "buttons": [
                                    'excelHtml5'
                                ]
                            });

                            table.on('order.dt search.dt', function () {
                                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1;
                                    table.cell(cell).invalidate('dom');
                                });
                            }).draw();

                            //attendance checking
                            //if (dtId == "#member-rpt-example") {

                            //    table.columns().every(function () {
                            //        if (this.header().innerHTML == "Attendance Status") {
                            //            table.column(11, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                            //                var days = parseInt(cell.innerHTML);
                            //                if (days >= 0 && days <= 2) {
                            //                    cell.innerHTML = "<span class='attendance-active'>Active</span>";
                            //                }
                            //                else if (days >= 3 && days <= 10) {
                            //                    cell.innerHTML = "<span class='attendance-mod-active'>Moderately Active</span>";
                            //                }
                            //                else {
                            //                    cell.innerHTML = "<span class='attendance-in-active'>Inactive</span>";
                            //                }
                            //                table.cell(cell).invalidate('dom');
                            //            });
                            //        }
                            //    }).draw();

                            //    var cnt = 0;
                            //    table.rows().every(function () {
                            //        var row = $(this.node());
                            //        var attendanceStatus = row.find('td').eq(11).text();
                            //        console.log(attendanceStatus);
                            //        if (attendanceStatus == "Active") {
                            //            row.find("td:visible").each(function () {
                            //                $(this).addClass('attendance-active');
                            //            });
                            //        }
                            //        else if (attendanceStatus == "Moderately Active") {
                            //            row.find("td:visible").each(function () {
                            //                $(this).addClass('attendance-mod-active');
                            //            });
                            //        }
                            //        else if (attendanceStatus == "Inactive") {
                            //            row.find("td:visible").each(function () {
                            //                $(this).addClass('attendance-in-active');
                            //            });
                            //        }
                            //        this.invalidate();
                            //    }).draw();

                            //    console.log("count : " + cnt);

                            //}

                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn');
                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn-primary');
                            $('.dt-button.buttons-excel.buttons-html5').text('Export');
                            $('.dt-buttons').css("float", "left");
                        }
                    })
                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }

    //evt - event
    //dtId - datatable id
    //formId - form id
    //apiName - api name
    //controllerName - controller to be invoked on button click
    //col - col to which the button to be added
    LoadDatatableAll = function (dtId, formId, apiName, controllerName, col) {

        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (result, textStatus, jqXHR) {

                console.log(result);

                //clear datatable if datatable is not empty
                if ($(dtId).DataTable().data().any()) {
                    $(dtId).empty();
                }

                //Check the length of the result data from api
                if (result == undefined || result == null || result.length == 0) {
                    $(dtId).DataTable({
                        "oLanguage": {
                            "sEmptyTable": "No Data Found."
                        },
                        "destroy": true,
                        "bLengthChange": false
                    });
                }
                else {

                    //get property names from first object of an array of object
                    var keys = Object.keys(result[0]);

                    var jsonObj = [];
                    item = {};
                    item["data"] = 'id';
                    item["defaultContent"] = '';
                    item["sortable"] = false;
                    jsonObj.push(item);

                    var i = 0;
                    //iterate over the property names to create columns entry of datatable
                    keys.forEach(function (value) {

                        i += 1;
                        item = {}
                        item["data"] = value;
                        item["autoWidth"] = true;

                        jsonObj.push(item);

                        //if the column creation of datatable is completed
                        if (i == keys.length) {

                            var columns = JSON.stringify(jsonObj);

                            console.log(columns);

                            var table = $(dtId).DataTable({
                                "aaData": result,
                                "destroy": true,
                                "columns": jsonObj,
                                "order": [],
                                "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, 'All']],
                                "columnDefs": [
                                    { orderable: false, targets: [0] },
                                    {
                                        orderable: false, targets: [col], render: function (data, type, row, meta) {
                                            return '<a href="' + controllerName + '' + row.Id + '"><i class="fa fa-pencil-square-o"></i></a>';

                                        }
                                    }
                                ]
                            });

                            table.on('order.dt search.dt', function () {
                                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1;
                                    table.cell(cell).invalidate('dom');
                                });
                            }).draw();

                        }
                    })
                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }

    //dtId - datatable id
    //apiName - api name
    LoadNotification = function (dtId, paramname, params, apiName) {

        var requestData = {};

        requestData[paramname] = params;

        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            data: JSON.stringify(requestData),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (result, textStatus, jqXHR) {

                console.log(result);

                //clear datatable if datatable is not empty
                if ($(dtId).DataTable().data().any()) {
                    $(dtId).empty();
                }

                //Check the length of the result data from api
                if (result == undefined || result == null || result.length == 0) {
                    $(dtId).DataTable({
                        "oLanguage": {
                            "sEmptyTable": "No Data Found."
                        },
                        "destroy": true,
                        "bLengthChange": false
                    });
                }
                else {

                    //get property names from first object of an array of object
                    var keys = Object.keys(result[0]);

                    var jsonObj = [];
                    item = {};
                    item["data"] = 'id';
                    item["defaultContent"] = '';
                    item["sortable"] = false;
                    jsonObj.push(item);

                    var i = 0;
                    //iterate over the property names to create columns entry of datatable
                    keys.forEach(function (value) {

                        i += 1;
                        item = {}
                        item["data"] = value;
                        item["autoWidth"] = true;

                        jsonObj.push(item);

                        //if the column creation of datatable is completed
                        if (i == keys.length) {

                            var columns = JSON.stringify(jsonObj);

                            console.log(columns);

                            var table = $(dtId).DataTable({
                                "aaData": result,
                                "destroy": true,
                                "columns": jsonObj,
                                "order": [],
                                "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, 'All']],
                                "columnDefs": [
                                    { orderable: false, targets: [0] }
                                ],
                                "dom": 'Blfrtip',
                                "buttons": [
                                    'excelHtml5'
                                ]
                            });

                            table.on('order.dt search.dt', function () {
                                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1;
                                    table.cell(cell).invalidate('dom');
                                });
                            }).draw();

                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn');
                            $('.dt-button.buttons-excel.buttons-html5').addClass('btn-primary');
                            $('.dt-button.buttons-excel.buttons-html5').text('Export');
                            $('.dt-buttons').css("float", "left");
                        }
                    })

                }

            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }

    //Load birthday todays total
    LoadBDtodaysTotal = function (spanId, apiName) {
        //ajax call to load datatable
        $.ajax({
            type: "POST",
            url: apiName,
            dataType: 'json',
            success: function (result, textStatus, jqXHR) {
                console.log(result);
                console.log(spanId);
                $('#' + spanId).text(result);
            },
            error: function (data, textStatus, jqXHR) {
                console.log("ajax error");
                console.log(data.responseText);
            }
        });
    }

    //Invoice or member delete
    InvoiceMemberDelete = function (e, apiName, paramname, params, id) {

        e.preventDefault();

        if (confirm("Are you sure want to delete " + id + " ?")) {

            var requestData = {};

            requestData[paramname] = $(params).val();

            console.log(JSON.stringify(requestData));

            //ajax call to load datatable
            $.ajax({
                type: "POST",
                url: apiName,
                data: JSON.stringify(requestData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (result, textStatus, jqXHR) {
                    console.log(result);
                    $(params).val('');
                    alert(id + " deleted.");
                },
                error: function (data, textStatus, jqXHR) {
                    alert(id + " not able to delete. Try later!");
                    console.log(data.responseText);
                }
            });
        }

        return false;

    }

    // Report div height set based on window size
    function thirty_pc() {
        var height = $(window).height();
        var thirtypc = height * (85 / 100);
        thirtypc = parseInt(thirtypc) + 'px';
        $("div.section_box1:first").css('height', thirtypc);
        $("div.section_box1:first").css('overflow-x', "auto");
    }

    $(document).ready(function () {
        thirty_pc();
        $(window).bind('resize', thirty_pc);
    });
});
