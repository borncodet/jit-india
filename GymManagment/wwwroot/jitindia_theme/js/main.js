
/*Init*/
(function ($) {
    $(function () {

        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $(".dropdown-button").dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });

    });
})(jQuery);

/*Evaluates the copyright year*/
;
(function ($) {
    $(document).ready(function () {
        var d = new Date();
        var n = d.getFullYear();
        $("#copyright-year").text(n);
    });
})(jQuery);


/*-----------OWL-----------*/
$('.main-slider').owlCarousel({
    animateOut: 'fadeOut',
    items:1, 
    smartSpeed:450,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:false,
    loop:true
    
});
/*-----------OWL-----------*/


$(document).ready(function(){
	$('.search-collate div.box-header-toggle').click(function(){
		if($(this).hasClass("active_search")){			
			$(this).next('.js-search-collate-facet').slideDown();
			$(this).removeClass('active_search');
		}
		else{				
			$(this).next('.js-search-collate-facet').slideUp();
			$(this).addClass('active_search');
		}	
	 });
}); 

$(function() {
	$( "#slider-range-min" ).slider({
	range: true,
	min: 0,
	max: 500,
	values: [ 75, 300 ],
	slide: function( event, ui ) {
	$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	}
	});
	$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
	" - $" + $( "#slider-range" ).slider( "values", 1 ) );
});