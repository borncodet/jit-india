﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GymManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class BaseController : ControllerBase
    {
		protected readonly IHttpContextAccessor _accessor;
		protected readonly ILogger _logger;

		public BaseController(IHttpContextAccessor accessor,
			ILogger<BaseController> logger)
		{
			_accessor = accessor;
		}
		
		protected string GetCurrentUserId()
		{
			return _accessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value?.Trim();
		}
	}
}