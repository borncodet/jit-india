﻿using GymManagment.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Shared.Helpers
{
	public interface IPagedList
	{
		int PageIndex { get; }
		int PageSize { get; }
		int TotalCount { get; }
		int TotalPages { get; }
		bool HasPreviousPage { get; }
		bool HasNextPage { get; }
	}

	
	public class PagedListViewModel<T> : List<T>
	{
		public int PageIndex { get; protected set; }
		public int PageSize { get; protected set; }
		public int TotalCount { get; protected set; }
		public int TotalPages { get; protected set; }

		public bool HasPreviousPage => PageIndex > 1;
		public bool HasNextPage => PageIndex < TotalPages;


		public PagedListViewModel(IEnumerable<T> items, int count, int pageIndex = 0, int pageSize = int.MaxValue)
		{
			PageSize = pageSize;
			PageIndex = pageIndex <= 1 ? 1 : pageIndex;

			TotalCount = count;
			TotalPages = count / pageSize;
			if (count % pageSize > 0)
			{
				TotalPages++;
			}
			AddRange(items);
		}
	}

	//public interface IPagedList<T>
	//{
	//	Task<PagedListCreation<T>> GetPagedListAsync(IQueryable<T> source, int pageIndex = 0, int pageSize = int.MaxValue);
	//}

	public class PagedList<T>//: IPagedList<T>
	{
		public async Task<PagedListViewModel<T>> GetPagedListAsync(IQueryable<T> source, int pageIndex = 0, int pageSize = int.MaxValue)
		{
			int page = pageIndex - 1;
			if (page <= 0)
			{
				page = 0;
			}
			var count = await source.CountAsync();
			var items = await source.Skip(page * pageSize).Take(pageSize).ToListAsync();
			var res= new PagedListViewModel<T>(items, count, pageIndex, pageSize);
			return res;
		}
	}
}

