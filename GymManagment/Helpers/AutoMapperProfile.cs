﻿using AutoMapper;
using GymManagment.Core.Models;
using GymManagment.Core.PostModels;

namespace GymManagment.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CMSContentViewPostModel, CMSContent>().ReverseMap();
            CreateMap<CMSContent, CMSContentViewPostModel>().ReverseMap();
        }
    }
}
