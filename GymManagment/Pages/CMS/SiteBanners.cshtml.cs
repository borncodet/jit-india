﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GymManagment.Core;
using GymManagment.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace GymManagment.Pages.CMS
{
    public class SiteBannersModel : PageModel
    {
		private readonly ApplicationDbContext _context;

		public SiteBannersModel(ApplicationDbContext context)
		{
			_context = context;
		}

		public List<CMSContent> Items { get; set; }

		public async Task OnGetAsync()
        { 
			Items = await _context.CMSContents.Where(i => i.CMSKey == "IndexBanner" || i.CMSKey == "AboutUsBanner" || i.CMSKey == "ProgramsBanner" || i.CMSKey == "FacilitiesBanner" || i.CMSKey == "ContactUsBanner" || i.CMSKey == "BlogBanner").ToListAsync();
		}
    }
}