﻿using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using JW;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class CareerModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;
        private IConfiguration _configuration { get; }


        public CareerModel(ICMSRepository cmsRepository, ApplicationDbContext context, IConfiguration configuration)
        {
            _cmsRepository = cmsRepository;
            _context = context;
            _configuration = configuration;
        }

        public IEnumerable<CMSContent> Jobs { get; set; }
        public Pager Pager { get; set; }
        public IList<CMSContentViewModel> Banner { get; set; }
        //[BindProperty]
        //public InputModel Input { get; set; }

        //[ModelBinder]
        //public class InputModel
        //{
        //    [BindProperty]
        //    public string JobTitle { get; set; }
        //    [BindProperty]
        //    public string JobLocation { get; set; }
        //}

        [BindProperty]
        public string JobTitle { get; set; }
        [BindProperty]
        public string JobLocation { get; set; }

        public async Task OnGetAsync(string title, string location, int p = 1)
        {
            if ((!string.IsNullOrEmpty(title) && !string.IsNullOrWhiteSpace(title)) &&
             (string.IsNullOrEmpty(location) || string.IsNullOrWhiteSpace(location)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(title.ToUpper())).ToList();
            }
            else if ((!string.IsNullOrEmpty(location) && !string.IsNullOrWhiteSpace(location)) &&
                (string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text3.ToUpper().Contains(location.ToUpper())).ToList();
            }
            else if ((string.IsNullOrEmpty(location) || string.IsNullOrWhiteSpace(location)) &&
               (string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs").ToList();
            }
            else
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(title.ToUpper()) && i.Text3.ToUpper().Contains(location.ToUpper())).ToList();
            }
            JobTitle = title;
            JobLocation = location;
            Pager = new Pager(Jobs.Count(), p, Convert.ToInt32(_configuration["Paging:PageSize"]), Convert.ToInt32(_configuration["Paging:maxPages"]));
            Jobs = Jobs.Skip((Pager.CurrentPage - Pager.PageSize) * 1).Take(Pager.PageSize);
            Banner = (await _cmsRepository.GetCMSContent("CareerBanner")).ToList();
        }

        public async Task<IActionResult> OnPostSaveAsync(int p = 1)
        {
            if ((!string.IsNullOrEmpty(JobTitle) && !string.IsNullOrWhiteSpace(JobTitle)) &&
                (string.IsNullOrEmpty(JobLocation) || string.IsNullOrWhiteSpace(JobLocation)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(JobTitle.ToUpper())).ToList();
            }
            else if ((!string.IsNullOrEmpty(JobLocation) && !string.IsNullOrWhiteSpace(JobLocation)) &&
                (string.IsNullOrEmpty(JobTitle) || string.IsNullOrWhiteSpace(JobTitle)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text3.ToUpper().Contains(JobLocation.ToUpper())).ToList();
            }
            else if ((string.IsNullOrEmpty(JobLocation) || string.IsNullOrWhiteSpace(JobLocation)) &&
               (string.IsNullOrEmpty(JobTitle) || string.IsNullOrWhiteSpace(JobTitle)))
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs").ToList();
            }
            else
            {
                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(JobTitle.ToUpper()) && i.Text3.ToUpper().Contains(JobLocation.ToUpper())).ToList();
            }
            Pager = new Pager(Jobs.Count(), p, Convert.ToInt32(_configuration["Paging:PageSize"]), Convert.ToInt32(_configuration["Paging:maxPages"]));
            Jobs = Jobs.Skip((Pager.CurrentPage - Pager.PageSize) * 1).Take(Pager.PageSize);
            Banner = (await _cmsRepository.GetCMSContent("CareerBanner")).ToList();
            return Page();
        }
    }
}

//if ((!string.IsNullOrEmpty(JobTitle) && !string.IsNullOrWhiteSpace(JobTitle)) &&
//                (string.IsNullOrEmpty(JobLocation) || string.IsNullOrWhiteSpace(JobLocation)))
//            {
//                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(JobTitle.ToUpper())).ToList();
//            }
//            else if ((!string.IsNullOrEmpty(JobLocation) && !string.IsNullOrWhiteSpace(JobLocation)) &&
//                (string.IsNullOrEmpty(JobTitle) || string.IsNullOrWhiteSpace(JobTitle)))
//            {
//                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text3.ToUpper().Contains(JobLocation.ToUpper())).ToList();
//            }
//            else if ((string.IsNullOrEmpty(JobLocation) || string.IsNullOrWhiteSpace(JobLocation)) &&
//               (string.IsNullOrEmpty(JobTitle) || string.IsNullOrWhiteSpace(JobTitle)))
//            {
//                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs").ToList();
//            }
//            else
//            {
//                Jobs = _context.CMSContents.Where(i => i.CMSKey == "Jobs" && i.Text2.ToUpper().Contains(JobTitle.ToUpper()) && i.Text3.ToUpper().Contains(JobLocation.ToUpper())).ToList();
//            }
//            if (!string.IsNullOrEmpty(JobLocation) && !string.IsNullOrWhiteSpace(JobLocation))
//            {
//                JobLocation = JobLocation;
//            }
//            if (!string.IsNullOrEmpty(JobTitle) && !string.IsNullOrWhiteSpace(JobTitle))
//            {
//                JobTitle = JobTitle;
//            }
