﻿using GymManagment.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
	public class BasePageModel: PageModel
	{
		protected readonly ApplicationDbContext _context;
		protected readonly IHttpContextAccessor _httpContextAccessor;
		protected readonly ILogger _logger;
		protected readonly IHostingEnvironment _env;

		public BasePageModel(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor, //ILogger logger, 
			IHostingEnvironment env)
		{
			_context = context;
			_httpContextAccessor = httpContextAccessor;
			//_logger = logger;
			_env = env;

			_context.CurrentUserId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
		}

		public string CurrentUserId { get { return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier); } }
	}
}
