﻿using GymManagment.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages.States
{
	[Authorize]
	public class EditModel : PageModel
	{
		private readonly GymManagment.Core.ApplicationDbContext _context;

		public EditModel(GymManagment.Core.ApplicationDbContext context)
		{
			_context = context;
		}

		[BindProperty]
		public State State { get; set; }

		public async Task<IActionResult> OnGetAsync(string id)
		{
			if (id == null)
			{
				return NotFound();
			}

			State = await _context.States
				.Include(s => s.Country).FirstOrDefaultAsync(m => m.Id == id);

			if (State == null)
			{
				return NotFound();
			}
			ViewData["CountryId"] = new SelectList(_context.Countries, "Id", "Name");
			return Page();
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			_context.Attach(State).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!StateExists(State.Id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return RedirectToPage("./Index");
		}

		private bool StateExists(string id)
		{
			return _context.States.Any(e => e.Id == id);
		}
	}
}
