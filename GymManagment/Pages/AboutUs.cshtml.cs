﻿using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class AboutUsModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public AboutUsModel(ApplicationDbContext context,
                             ICMSRepository cmsRepository)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public CMSContentViewModel AboutUsBottomFirstSection { get; set; }
        public CMSContentViewModel AboutUsBottomSecondSection { get; set; }
        public CMSContent Mission { get; set; }
        public CMSContent Vision { get; set; }
        public CMSContent CoreValue { get; set; }
        public CMSContent Team { get; set; }
        public async Task OnGetAsync()
        {
            Mission = await _context.CMSContents.Where(c => c.CMSKey == "Mission").FirstOrDefaultAsync();
            Vision = await _context.CMSContents.Where(c => c.CMSKey == "Vision").FirstOrDefaultAsync();
            CoreValue = await _context.CMSContents.Where(c => c.CMSKey == "CoreValue").FirstOrDefaultAsync();
            Team = await _context.CMSContents.Where(c => c.CMSKey == "Team").FirstOrDefaultAsync();
            AboutUsBottomFirstSection = (await _cmsRepository.GetCMSContent("AboutUsBottomFirstSection")).ToList().FirstOrDefault();
            AboutUsBottomSecondSection = (await _cmsRepository.GetCMSContent("AboutUsBottomSecondSection")).ToList().FirstOrDefault();
        }
    }
}
