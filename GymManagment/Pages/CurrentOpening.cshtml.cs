﻿using GymManagment.Core;
using GymManagment.Core.Models;
using GymManagment.Core.ViewModels;
using GymManagment.Helpers;
using GymManagment.Repository;
using GymManagment.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Pages
{
    public class CurrentOpeningModel : PageModel
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;
        private readonly IEmailer _emailSender;
        private SmtpConfig _config;

        public IList<NewsViewModel> News { get; set; }
        public CurrentOpeningModel(
            ICMSRepository cmsRepository,
            ApplicationDbContext context,
            IEmailer emailSender,
            IOptions<SmtpConfig> config)
        {
            _cmsRepository = cmsRepository;
            _context = context;
            _emailSender = emailSender;
            _config = config.Value;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            public string FirstName { get; set; }
            [Required]
            public string LastName { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            [Required]
            [EmailAddress]
            public string Email { get; set; }
            [Required]
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            public string JobId { get; set; }
            public string JobType { get; set; }
            public string JobTitle { get; set; }
            public string JobLocation { get; set; }
            [Required(ErrorMessage = "Please upload your resume")]
            public IFormFile Image { get; set; }
        }

        public bool SentMail = false;

        public CMSContent Job { get; set; }


        public async Task OnGetAsync(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Job = _context.CMSContents.Where(i => i.Id == id).FirstOrDefault();
            }
        }

        public async Task<IActionResult> OnPostSaveAsync()
        {
            var img = Input.Image;
            var body = $"{Input.Email} has uploaded the details successfully!";
            if (!string.IsNullOrEmpty(Input.JobId) && !string.IsNullOrWhiteSpace(Input.JobId))
            {
                body += "<br><b>Job Details :</b><br><b>Job Id :</b>" + Input.JobId;
                body += "<br><b>Job Type :</b>" + Input.JobType;
                body += "<br><b>Job Title :</b>" + Input.JobTitle;
                body += "<br><b>Job Location :</b>" + Input.JobLocation;
            }
            body += $"<br><b>Address :</b><br>{ Input.FirstName} { Input.LastName}";
            if (!string.IsNullOrEmpty(Input.Address) && !string.IsNullOrWhiteSpace(Input.Address))
            {
                body += "<br>" + Input.Address;
            }
            if (!string.IsNullOrEmpty(Input.City) && !string.IsNullOrWhiteSpace(Input.City))
            {
                body += "<br>" + Input.City;
            }

            if (!string.IsNullOrEmpty(Input.State) && !string.IsNullOrWhiteSpace(Input.State))
            {
                body += "<br>" + Input.State;
            }

            if (!string.IsNullOrEmpty(Input.PostalCode) && !string.IsNullOrWhiteSpace(Input.PostalCode))
            {
                body += "<br>" + Input.PostalCode;
            }

            if (!string.IsNullOrEmpty(Input.Country) && !string.IsNullOrWhiteSpace(Input.Country))
            {
                body += "<br>" + Input.Country;
            }

            body += "<br><b>Email :</b>" + Input.Email;
            body += "<br><b>PhoneNumber :</b>" + Input.PhoneNumber;

            (bool success, string errorMsg) result = await _emailSender.SendEmailAsync(Input.Email, Input.Email, _config.Name, _config.EmailAddress, "Account Registration", body, Input.Image);
            if (result.success)
            {
                SentMail = true;
            }
            return Page();
        }
    }
}
