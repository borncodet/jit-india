﻿using GymManagment.Core;
using GymManagment.Core.ViewModels;
using GymManagment.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.ViewComponents
{
    public class LearningBannerViewComponent : ViewComponent
    {
        private readonly ICMSRepository _cmsRepository;
        private readonly ApplicationDbContext _context;

        public LearningBannerViewComponent(ICMSRepository cmsRepository, ApplicationDbContext context)
        {
            _cmsRepository = cmsRepository;
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string key)
        {
            CMSContentSocialViewModel model = new CMSContentSocialViewModel
            {
                CMSContentViewModel = (await _cmsRepository.GetCMSContent(key)).ToList()
            };
            return View(model);
        }
    }
}
