﻿using GymManagment.Core.ViewModels;
using GymManagment.Repository.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace GymManagment.Repository
{
    public class MediaRepository : IMediaRepository
    {
        private readonly IDataService _dataService;
        private readonly IHostingEnvironment _env;

        public MediaRepository(IDataService dataService, IHostingEnvironment env)
        {
            _dataService = dataService;
            _env = env;
        }

        public async Task<string> Save(string userId, UploadFilePostModel model)
        {
            string mediaId = await _dataService.ExecuteScalarAsync($@"INSERT INTO Medias
                    ( Id, CreatedBy, CreatedOn, IsActive, FileName, Extension, ContentType, ContentLength)
                    OUTPUT inserted.Id 
                    VALUES (NEWID(),'{userId}',GETDATE(), 1, @FileName, @Extension, @ContentType, @ContentLength)", model);
            return mediaId;
        }

        public async Task<ImagesSaveViewModel> SaveMedia(string userId, IFormFile file, string folderName = "")
        {
            var result = new ImagesSaveViewModel();
            folderName = "Gallery/" + folderName;

            try
            {
                if (file.Length > 0)
                {
                    if (!Directory.Exists(Path.Combine("wwwroot", folderName)))
                    {
                        Directory.CreateDirectory(Path.Combine("wwwroot", folderName));
                    }
                    string fileId = Guid.NewGuid().ToString("N");
                    string extension = Path.GetExtension(file.FileName).Substring(1);
                    string fileName = $"{fileId}.{extension}";
                    string path = Path.Combine(_env.ContentRootPath, "wwwroot", folderName);
                    using (var fs = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                    {
                        await file.CopyToAsync(fs);
                    }

                    string mediaId = await _dataService.ExecuteScalarAsync($@"INSERT INTO Medias
                    ( Id, CreatedBy, CreatedOn, IsActive, FileName, Extension, ContentType, ContentLength)
                    OUTPUT inserted.Id 
                    VALUES (NEWID(),'{userId}',GETDATE(), 1, '{"/" + folderName + "/" + fileName}', '{extension}', '{file.ContentType}',{file.Length})");
                    result.IsSuccess = true;
                    result.MediaID = mediaId;
                }
                else
                {
                    result.IsSuccess = false;
                }
            }
            catch
            {
                result.IsSuccess = false;
            }
            return result;
        }
    }
}
