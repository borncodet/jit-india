﻿using Dapper;
using GymManagment.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GymManagment.Repository
{
    public partial class DataService : IDataService, IDataService2
    {
        protected IDbConnection con;
        private string _conString;
        public DataService(string conString)
        {
            _conString = conString;
            CreateConnection();
        }

        protected IDbConnection CreateConnection()
        {
            con = new SqlConnection(_conString);
            return con;
        }

        public IDbConnection GetDbConnection()
        {
            return con;
        }

        public virtual async Task<TEntity> GetAsync<TEntity>(string query)
        {
            var result = await con.QueryAsync<TEntity>(query);
            return result.FirstOrDefault();
        }

        public virtual async Task<TEntity> GetAsync<TEntity>(string query, IDbTransaction transaction = null)
        {
            var result = await con.QueryAsync<TEntity>(query, transaction: transaction);
            return result.FirstOrDefault();
        }

        public virtual async Task<TEntity> GetAsync<TEntity>(string query, object param)
        {
            var result = await con.QueryAsync<TEntity>(query, param);
            return result.FirstOrDefault();
        }

        public virtual async Task<IEnumerable<TEntity>> GetEnumerableAsync<TEntity>(string query)
        {
            try
            {
                var result = await con.QueryAsync<TEntity>(query);
                return result.ToList();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetEnumerableAsync<TEntity>(string query, object Param)
        {
            var result = await con.QueryAsync<TEntity>(query, Param);
            return result.ToList();
        }

        public IEnumerable<TEntity> GetEnumerable<TEntity>(string query)
        {
            var result = con.Query<TEntity>(query);
            return result.ToList();
        }

        public IEnumerable<TEntity> GetEnumerable<TEntity>(string query, object param)
        {
            var result = con.Query<TEntity>(query, param);
            return result.ToList();
        }

        public async Task<int> InsertOrUpdateAsync(string query)
        {
            return await con.ExecuteAsync(query);
        }

        public virtual async Task<int> InsertOrUpdateAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null)
        {
            return await con.ExecuteAsync(query, model, transaction);
        }


        public virtual async Task<string> ExecuteScalarAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null)
        {
            return await con.ExecuteScalarAsync<string>(query, model, transaction);
        }

        public virtual async Task<string> ExecuteScalarAsync(string query, IDbTransaction transaction = null)
        {
            return await con.ExecuteScalarAsync<string>(query, transaction: transaction);
        }

        public int ExecuteScalar(string query)
        {
            return con.ExecuteScalar<int>(query);
        }


        public virtual async Task<int> ExecuteAsync<TEntity>(string query, TEntity model, IDbTransaction transaction = null)
        {
            return await con.ExecuteAsync(query, model, transaction);
        }

        public virtual async Task<int> DeleteAsync(string sp_name, object param)
        {
            try
            {
                return await con.ExecuteAsync(sp_name, param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        //public async Task<PagedList<T>> GetPagedList<T>(string query, int pageIndex = 0, int pageSize = int.MaxValue)
        //{
        //  PagedList<T> result = new PagedList<T>();
        //  result.PageSize = pageSize;
        //  result.PageIndex = pageIndex <= 1 ? 1 : pageIndex;

        //  var orderbyIndex = query.IndexOf(@"Order by", StringComparison.CurrentCultureIgnoreCase);
        //  string countQuery = $"Select Count(*) as Cnt From({ query.Substring(0, orderbyIndex) }) As A";

        //  int total = ExecuteScalar(countQuery);
        //  result.TotalCount = total;
        //  result.TotalPages = total / pageSize;
        //  if (total % pageSize > 0)
        //  {
        //      result.TotalPages++;
        //  }
        //  query += $" OFFSET { (result.PageIndex - 1) * pageSize } ROWS FETCH NEXT { pageSize } ROWS ONLY";
        //  result.Data = await GetEnumerableAsync<T>(query);
        //  return result;
        //}



    }
}
