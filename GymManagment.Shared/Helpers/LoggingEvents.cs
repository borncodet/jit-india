﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Helpers
{
	public static class LoggingEvents
	{
		public static readonly EventId INIT_DATABASE = new EventId(101, "Error while creating and seeding database");
		public static readonly EventId SEND_EMAIL = new EventId(201, "Error while sending email");

		public static readonly EventId InternalError = new EventId(701, "Error while create user");
	}
}
