﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IBaseEntity
	{
		string Id { get; set; }
	}
}
