﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IHostEntity
	{
		string Host { get; set; }
	}
}
