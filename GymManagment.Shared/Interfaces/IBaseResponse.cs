﻿using GymManagment.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Interfaces
{
	public interface IBaseResponse
	{
		string Message { get; set; }
		ResponseType Type { get; set; }
		bool IsSuccess { get; set; }
		ResponseCode Code { get; set; }
	}
}
