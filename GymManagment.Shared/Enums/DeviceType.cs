﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GymManagment.Shared.Enums
{
	public enum DeviceType
	{
		WebBrowser = 1,
		MobileBrowser=2,
		Ios=3,
		Android=4
	}
}
